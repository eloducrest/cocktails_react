import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {Splide, SplideSlide} from '@splidejs/react-splide';
import '@splidejs/splide/css/sea-green';

export default function CocktailDetails() {
  const {cocktailId} = useParams();

  const [ingredients, setIngredients] = useState([]);
  const [cocktail, setCocktail] = useState([]);
  const [measures, setMeasures] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/lookup.php?i=${cocktailId}`)
      .then((res) => res.json())
      .then((data) => {
        setCocktail(data.drinks[0]);
      });
  }, [cocktailId]);

  useEffect(() => {
    const getMeasures = () => {
      let tmpMeasures = [];
      for (let i = 1; i <= 15; i++) {
        const propName = `strMeasure${i}`;
        if (cocktail[propName] !== null) {
          tmpMeasures.push(cocktail[propName]);
        }
      }
      setMeasures(tmpMeasures)
    }
    const getIngredients = () => {
      let tmpIngredients = [];
      for (let i = 1; i <= 15; i++) {
        const propName = `strIngredient${i}`;
        if (cocktail[propName] !== null) {
          tmpIngredients.push(cocktail[propName]);
        }
      }
      setIngredients(tmpIngredients)
    }
    if (cocktail) {
      getMeasures();
      getIngredients();
    }
  }, [cocktail]);

  return (
    <>
      <div className={'mb-8'}>
        <Link to="/cocktails">
          <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
            <path
              d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/>
          </svg>
        </Link>
      </div>
      <div className="container mx-auto">
        <div className="block p-6 bg-white border border-gray-200 rounded-lg shadow">
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-y-4 md:gap-x-16">
            <img src={cocktail.strDrinkThumb} alt={`of ${cocktail.strDrink}`}/>
            <div className="lg:col-span-2 flex flex-col gap-y-4">
              <h1 className="font-bold text-2xl lg:text-4xl text-gray-900 underline">{cocktail.strDrink}</h1>
              <p className="font-bold text-gray-700 text-xl">Category : <span
                className="font-normal text-gray-600 text-lg">{cocktail.strCategory}</span></p>
              <p className="font-bold text-gray-700 text-xl">Glass type : <span
                className="font-normal text-gray-600 text-lg">{cocktail.strGlass}</span></p>
              <p className="font-bold text-gray-700 text-xl">Alcoholic cocktail ? <span
                className="font-normal text-gray-600 text-lg">{cocktail.strAlcoholic}</span></p>
              <div>
                <p className="font-bold text-gray-700 text-xl">Ingredients :</p>
                <ul className="font-normal text-gray-600 text-lg list-decimal">
                  {ingredients.map((ingredient, i) =>
                    <li className="ml-6" key={i}>{ingredient}</li>
                  )}
                </ul>
              </div>
              <div>
                <p className="font-bold text-gray-700 text-xl">Measures :</p>
                <ul className="font-normal text-gray-600 text-lg list-decimal">
                  {measures.map((measure, i) =>
                    <li className="ml-6" key={i}>{measure}</li>
                  )}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="container mx-auto mt-8">
          <h2 className="font-bold text-4xl">Ingredients for {cocktail.strDrink}</h2>
          <Splide options={{
            perPage: 3,
            gap: '1rem',
          }}>
            {ingredients.map((ingredient, i) =>
              <SplideSlide key={i}>
                <div>
                  <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow">
                    <Link to={`/ingredient/${ingredient}`}>
                      <img className="rounded-t-lg" src={`https://thecocktaildb.com/images/ingredients/${ingredient}-Medium.png`} alt=""/>
                    </Link>
                    <div className="p-5">
                      <Link to={`/ingredient/${ingredient}`}>
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{ingredient}</h5>
                      </Link>
                      <Link to={`/ingredient/${ingredient}`}
                            className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                        Show ingredient
                        <svg className="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                             fill="none" viewBox="0 0 14 10">
                          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"/>
                        </svg>
                      </Link>
                    </div>
                  </div>
                </div>
              </SplideSlide>
            )}
          </Splide>
        </div>
      </div>
    </>
  )
}
