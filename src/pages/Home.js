import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

export default function Home() {

  const [randomCocktail, setRandomCocktail] = useState("");

  useEffect(() => {
    getRandomCocktail();
  }, []);

  const getRandomCocktail = () => {
    fetch(`${process.env.REACT_APP_API_URL}/random.php`)
      .then((res) => res.json())
      .then((data) => setRandomCocktail(data.drinks[0]));
  }

  return (
    <>
      <div className="flex items-center">
        <div className="cursor-pointer">
          <svg onClick={getRandomCocktail} xmlns="http://www.w3.org/2000/svg" height="4em" viewBox="0 0 640 512">
            <path d="M274.9 34.3c-28.1-28.1-73.7-28.1-101.8 0L34.3 173.1c-28.1 28.1-28.1 73.7 0 101.8L173.1 413.7c28.1 28.1 73.7 28.1 101.8 0L413.7 274.9c28.1-28.1 28.1-73.7 0-101.8L274.9 34.3zM200 224a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zM96 200a24 24 0 1 1 0 48 24 24 0 1 1 0-48zM224 376a24 24 0 1 1 0-48 24 24 0 1 1 0 48zM352 200a24 24 0 1 1 0 48 24 24 0 1 1 0-48zM224 120a24 24 0 1 1 0-48 24 24 0 1 1 0 48zm96 328c0 35.3 28.7 64 64 64H576c35.3 0 64-28.7 64-64V256c0-35.3-28.7-64-64-64H461.7c11.6 36 3.1 77-25.4 105.5L320 413.8V448zM480 328a24 24 0 1 1 0 48 24 24 0 1 1 0-48z"/>
          </svg>
          <p>Random cocktail !</p>
        </div>
        <div>
          <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow">
            <Link to={`/cocktail/${randomCocktail.idDrink}`} state={{ cocktail: randomCocktail }}>
              <img className="rounded-t-lg" src={randomCocktail.strDrinkThumb} alt="" />
            </Link>
            <div className="p-5">
              <Link to={`/cocktail/${randomCocktail.idDrink}`} state={{ cocktail: randomCocktail }}>
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{randomCocktail.strDrink}</h5>
              </Link>
              <p className="mb-3 font-normal text-gray-700">{randomCocktail.strCategory}</p>
              <Link to={`/cocktail/${randomCocktail.idDrink}`} state={{ cocktail: randomCocktail }}
                    className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                Voir le cocktail
                <svg className="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                </svg>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
