import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {Splide, SplideSlide} from '@splidejs/react-splide';
import '@splidejs/splide/css/sea-green';

export default function IngredientDetails() {
  const [ingredient, setIngredient] = useState([]);
  const [cocktailsByIngredient, setCocktailsByIngredient] = useState([]);
  const {name} = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/search.php?i=${name}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        setIngredient(data.ingredients[0])
      });

    fetch(`${process.env.REACT_APP_API_URL}/filter.php?i=${name}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        setCocktailsByIngredient(data.drinks)
      });


  }, [])

  return (
    <>
      <div className={'mb-8'}>
        <Link to="/ingredients">
          <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
            <path
              d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/>
          </svg>
        </Link>
      </div>
      <div className="container mx-auto">
        <div className="block p-6 bg-white border border-gray-200 rounded-lg shadow">
          <div className="flex flex-col gap-y-4">
            <h1 className="font-bold text-2xl lg:text-4xl text-gray-900 underline">{ingredient.strIngredient}</h1>
            <p className="font-bold text-gray-700 text-xl">Type : <span
              className="font-normal text-gray-600 text-lg">{ingredient.strType}</span></p>
            <p className="font-bold text-gray-700 text-xl">Alcohol ? <span
              className="font-normal text-gray-600 text-lg">{ingredient.strAlcohol}</span></p>
            <p className="font-bold text-gray-700 text-xl">Description : <span
              className="font-normal text-gray-600 text-lg">{ingredient.strDescription}</span></p>
          </div>
        </div>

        <div className="container mx-auto mt-8">
          <h2 className="font-bold text-4xl">Cocktails with {ingredient.strIngredient}</h2>
          <Splide options={{
            rewind: true,
            perPage: 3,
            perMove: 1,
            padding: { left: '2rem', right: '0rem' }
          }}>
            {cocktailsByIngredient.map(cocktail =>
              <SplideSlide key={cocktail.idDrink}>
                <div>
                  <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow">
                    <Link to={`/cocktail/${cocktail.idDrink}`} state={{cocktail: cocktail}}>
                      <img className="rounded-t-lg" src={cocktail.strDrinkThumb} alt=""/>
                    </Link>
                    <div className="p-5">
                      <Link to={`/cocktail/${cocktail.idDrink}`} state={{cocktail: cocktail}}>
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{cocktail.strDrink}</h5>
                      </Link>
                      <p className="mb-3 font-normal text-gray-700">{cocktail.strCategory}</p>
                      <Link to={`/cocktail/${cocktail.idDrink}`} state={{cocktail: cocktail}}
                            className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                        Voir le cocktail
                        <svg className="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                             fill="none" viewBox="0 0 14 10">
                          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"/>
                        </svg>
                      </Link>
                    </div>
                  </div>
                </div>
              </SplideSlide>
            )}
          </Splide>
        </div>
      </div>
    </>
  )
}
