import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom";

export default function Ingredients() {

  const [ingredients, setIngredients] = useState([]);

  useEffect(() => {
    getIngredientsList();
  }, []);

  const getIngredientsList = () => {
    fetch(`${process.env.REACT_APP_API_URL}/list.php?i=list`)
      .then((res) => res.json())
      .then((data) => setIngredients(data.drinks));
  }

  return (
    <>
      <div className="container mx-auto">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
          {ingredients.map((ingredient, i) => (
            <div key={i}>
              <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow">
                <div className="p-5">
                  <Link to={`/ingredient/${ingredient.strIngredient1}`}>
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{ingredient.strIngredient1}</h5>
                  </Link>
                  <Link to={`/ingredient/${ingredient.strIngredient1}`}
                        className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                    Show ingredient
                    <svg className="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                      <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                    </svg>
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
