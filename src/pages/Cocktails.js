import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom";

export default function Cocktails() {

  const [cocktails, setCocktails] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/search.php?s=${searchValue}`)
      .then((res) => res.json())
      .then((data) => setCocktails(data.drinks));
  }, [searchValue]);

  return (
    <>
      <div className="mb-8">
        <label htmlFor="default-search" className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
        <div className="relative">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
            </svg>
          </div>
          <input type="search" id="default-search" className="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                 placeholder="Search cocktails by name"
                 onChange={(e) => setSearchValue(e.target.value)}
                 required />
        </div>
      </div>
      <div className="container mx-auto">
        <div className="grid grid-cols-3 gap-4">
          {cocktails.map((cocktail) => (
            <div key={cocktail.idDrink}>
              <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow">
                <Link to={`/cocktail/${cocktail.idDrink}`}>
                  <img className="rounded-t-lg" src={cocktail.strDrinkThumb} alt="" />
                </Link>
                <div className="p-5">
                  <Link to={`/cocktail/${cocktail.idDrink}`}>
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{cocktail.strDrink}</h5>
                  </Link>
                  <p className="mb-3 font-normal text-gray-700">{cocktail.strCategory}</p>
                  <Link to={`/cocktail/${cocktail.idDrink}`}
                        className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                    Voir le cocktail
                    <svg className="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                      <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                    </svg>
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
