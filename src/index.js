import React from 'react';
import * as ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import reportWebVitals from './reportWebVitals';
import './index.css';
import App from "./App";
import Home from "./pages/Home";
import Cocktails from "./pages/Cocktails";
import CocktailDetails from "./pages/CocktailDetails";
import Ingredients from "./pages/Ingredients";
import IngredientDetails from "./pages/IngredientDetails"
import ErrorPage from "./pages/error-page";

const router = createBrowserRouter([
  {
    element: <App/>,
    errorElement: <ErrorPage/>,
    children: [
      {
        path: "/",
        element: <Home/>,
      },
      {
        path: "/cocktails",
        element: <Cocktails/>,
      },
      {
        path: "/cocktail/:cocktailId",
        element: <CocktailDetails/>,
        // with this data loaded before rendering
      },
      {
        path: "/ingredients",
        element: <Ingredients/>,
      },
      {
        path: "/ingredient/:name",
        element: <IngredientDetails/>,
      }
    ]
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>
);

reportWebVitals();
